EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "TransistorTester for Arduino Pro Mini"
Date "2020-06-07"
Rev "1"
Comp "P. Kronenwetter"
Comment1 ""
Comment2 "svn://mikrocontroller.net/transistortester"
Comment3 "https://easyeda.com/wagiminator/y-atmega-transistortester-smd"
Comment4 "https://www.mikrocontroller.net/articles/AVR_Transistortester"
$EndDescr
$Comp
L Device:R R3
U 1 1 5EE019B4
P 4600 1350
F 0 "R3" H 4670 1396 50  0000 L CNN
F 1 "27k" H 4670 1305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4530 1350 50  0001 C CNN
F 3 "~" H 4600 1350 50  0001 C CNN
	1    4600 1350
	1    0    0    -1  
$EndComp
Text HLabel 5000 3300 2    50   Input ~ 0
ADC4(SDA)
$Comp
L Reference_Voltage:LM4040DBZ-2.5 U2
U 1 1 5EE0B944
P 4500 3300
F 0 "U2" H 4500 3516 50  0000 C CNN
F 1 "LM4040DBZ-2.5" H 4500 3425 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4500 3100 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm4040-n.pdf" H 4500 3300 50  0001 C CIN
	1    4500 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3300 4350 3300
Wire Wire Line
	4650 3300 4900 3300
$Comp
L Device:R R10
U 1 1 5EE0EC1D
P 4900 3100
F 0 "R10" H 4970 3146 50  0000 L CNN
F 1 "2.2k" H 4970 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4830 3100 50  0001 C CNN
F 3 "~" H 4900 3100 50  0001 C CNN
	1    4900 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3250 4900 3300
Connection ~ 4900 3300
Wire Wire Line
	4900 3300 5000 3300
Wire Wire Line
	4900 2850 4900 2950
$Comp
L Device:R R9
U 1 1 5ECFCB7F
P 6750 1700
F 0 "R9" V 6850 1700 50  0000 L CNN
F 1 "470k" V 6850 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6680 1700 50  0001 C CNN
F 3 "~" H 6750 1700 50  0001 C CNN
	1    6750 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 1400 6100 1500
Connection ~ 6100 1400
Wire Wire Line
	6200 1400 6100 1400
Wire Wire Line
	6900 2300 6900 1700
Wire Wire Line
	6800 2300 6800 2000
Wire Wire Line
	6300 2300 6300 2200
Wire Wire Line
	6100 2300 6100 1950
Wire Wire Line
	5700 2300 5700 2000
Wire Wire Line
	5600 1700 5600 2300
Text HLabel 6900 2300 3    50   Input ~ 0
D13(SCK)
Text HLabel 6800 2300 3    50   Input ~ 0
D12(MISO)
Text HLabel 6300 2300 3    50   Input ~ 0
D11(MOSI)
Text HLabel 6100 2300 3    50   Input ~ 0
D10(SS)
Text HLabel 5700 2300 3    50   Input ~ 0
D9
Text HLabel 5600 2300 3    50   Input ~ 0
D8
Wire Wire Line
	6600 1400 6600 1700
Connection ~ 6600 1400
Wire Wire Line
	6650 1400 6600 1400
Text HLabel 6650 1400 2    50   Input ~ 0
ADC2
Wire Wire Line
	6300 1500 6300 1900
Text HLabel 6200 1400 2    50   Input ~ 0
ADC1
Wire Wire Line
	5900 1400 5900 1300
Connection ~ 5900 1400
Wire Wire Line
	5850 1400 5900 1400
Text HLabel 5850 1400 0    50   Input ~ 0
ADC0
Wire Wire Line
	6500 1400 6500 2000
Connection ~ 6500 1400
Wire Wire Line
	6500 1400 6600 1400
Wire Wire Line
	6500 1300 6500 1400
Connection ~ 6500 1300
Connection ~ 6400 1300
Wire Wire Line
	6400 1300 6500 1300
Connection ~ 6300 1300
Wire Wire Line
	6300 1300 6400 1300
Wire Wire Line
	6200 1300 6300 1300
Wire Wire Line
	6100 1500 6100 1650
Connection ~ 6100 1500
Wire Wire Line
	6100 1500 6300 1500
Connection ~ 6000 1300
Wire Wire Line
	6100 1300 6100 1400
$Comp
L Device:R R6
U 1 1 5ECFBC9F
P 6100 1800
F 0 "R6" H 6170 1846 50  0000 L CNN
F 1 "680" H 6170 1755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6030 1800 50  0001 C CNN
F 3 "~" H 6100 1800 50  0001 C CNN
	1    6100 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5ECFBF55
P 6300 2050
F 0 "R7" H 6370 2096 50  0000 L CNN
F 1 "470k" H 6370 2005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6230 2050 50  0001 C CNN
F 3 "~" H 6300 2050 50  0001 C CNN
	1    6300 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1300 6000 2000
Wire Wire Line
	5900 1300 6000 1300
Wire Wire Line
	5900 1700 5900 1400
$Comp
L Device:R R8
U 1 1 5ECFC900
P 6650 2000
F 0 "R8" V 6850 2000 50  0000 C CNN
F 1 "680" V 6750 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6580 2000 50  0001 C CNN
F 3 "~" H 6650 2000 50  0001 C CNN
	1    6650 2000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5ECFBA15
P 5850 2000
F 0 "R5" H 5920 2046 50  0000 L CNN
F 1 "470k" H 5920 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5780 2000 50  0001 C CNN
F 3 "~" H 5850 2000 50  0001 C CNN
	1    5850 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5ECFB4BF
P 5750 1700
F 0 "R4" H 5820 1746 50  0000 L CNN
F 1 "680" H 5820 1655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5680 1700 50  0001 C CNN
F 3 "~" H 5750 1700 50  0001 C CNN
	1    5750 1700
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x06_Female J1
U 1 1 5ECF5419
P 6200 1100
F 0 "J1" V 6138 712 50  0000 R CNN
F 1 "Test Connections" V 6047 712 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 6200 1100 50  0001 C CNN
F 3 "~" H 6200 1100 50  0001 C CNN
	1    6200 1100
	0    -1   -1   0   
$EndComp
$Comp
L TransistorTester-ProMini:OLED_0.96_I2C U3
U 1 1 5ED90E0F
P 6950 2900
F 0 "U3" H 6825 2915 50  0000 C CNN
F 1 "OLED_0.96_I2C" H 6825 2824 50  0000 C CNN
F 2 "TransistorTester-ProMini:OLED_0.96in_I2C-NoDrill" H 6950 2900 50  0001 C CNN
F 3 "" H 6950 2900 50  0001 C CNN
	1    6950 2900
	1    0    0    -1  
$EndComp
Text HLabel 7250 3150 2    50   Input ~ 0
GND
Text HLabel 7250 3250 2    50   Input ~ 0
VCC
Text HLabel 6400 3150 0    50   Input ~ 0
D5
Text HLabel 6400 3250 0    50   Input ~ 0
D2
Wire Wire Line
	7150 3150 7250 3150
Wire Wire Line
	7250 3250 7150 3250
Wire Wire Line
	6400 3150 6500 3150
Wire Wire Line
	6500 3250 6400 3250
$Comp
L Mechanical:MountingHole H1
U 1 1 5EDDF073
P 900 6500
F 0 "H1" H 1000 6546 50  0000 L CNN
F 1 "MountingHole" H 1000 6455 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 900 6500 50  0001 C CNN
F 3 "~" H 900 6500 50  0001 C CNN
	1    900  6500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5EDDF25A
P 900 6700
F 0 "H2" H 1000 6746 50  0000 L CNN
F 1 "MountingHole" H 1000 6655 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 900 6700 50  0001 C CNN
F 3 "~" H 900 6700 50  0001 C CNN
	1    900  6700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5EDE15B1
P 900 6900
F 0 "H3" H 1000 6946 50  0000 L CNN
F 1 "MountingHole" H 1000 6855 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 900 6900 50  0001 C CNN
F 3 "~" H 900 6900 50  0001 C CNN
	1    900  6900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5EDE15B7
P 900 7100
F 0 "H4" H 1000 7146 50  0000 L CNN
F 1 "MountingHole" H 1000 7055 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 900 7100 50  0001 C CNN
F 3 "~" H 900 7100 50  0001 C CNN
	1    900  7100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 5EDE3064
P 900 7300
F 0 "H5" H 1000 7346 50  0000 L CNN
F 1 "MountingHole" H 1000 7255 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 900 7300 50  0001 C CNN
F 3 "~" H 900 7300 50  0001 C CNN
	1    900  7300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 5EDE306A
P 900 7500
F 0 "H6" H 1000 7546 50  0000 L CNN
F 1 "MountingHole" H 1000 7455 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 900 7500 50  0001 C CNN
F 3 "~" H 900 7500 50  0001 C CNN
	1    900  7500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H7
U 1 1 5EE03E23
P 900 7700
F 0 "H7" H 1000 7746 50  0000 L CNN
F 1 "MountingHole" H 1000 7655 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 900 7700 50  0001 C CNN
F 3 "~" H 900 7700 50  0001 C CNN
	1    900  7700
	1    0    0    -1  
$EndComp
Text HLabel 4600 1100 1    50   Input ~ 0
VCC
Wire Wire Line
	4600 1100 4600 1200
Text HLabel 4900 2850 1    50   Input ~ 0
VCC
Text HLabel 4250 3300 0    50   Input ~ 0
GND
$Comp
L Connector:USB_B_Micro J2
U 1 1 5ED913EE
P 8000 1550
F 0 "J2" H 8057 2017 50  0000 C CNN
F 1 "USB_B_Micro" H 8057 1926 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex_47346-0001" H 8150 1500 50  0001 C CNN
F 3 "~" H 8150 1500 50  0001 C CNN
	1    8000 1550
	1    0    0    -1  
$EndComp
NoConn ~ 8300 1550
NoConn ~ 8300 1650
NoConn ~ 8300 1750
Wire Wire Line
	7900 1950 8000 1950
Connection ~ 8000 1950
Wire Wire Line
	8000 1950 8100 1950
Text HLabel 8100 1950 2    50   Input ~ 0
GND
Text HLabel 8350 1350 2    50   Input ~ 0
VCC
Wire Wire Line
	8350 1350 8300 1350
Text HLabel 3350 3100 2    50   Input ~ 0
RX
Text HLabel 3350 3200 2    50   Output ~ 0
D2
Text HLabel 3350 3300 2    50   UnSpc ~ 0
D3
Text HLabel 3350 3400 2    50   UnSpc ~ 0
D4
Text HLabel 3350 3500 2    50   Output ~ 0
D5
Text HLabel 3350 3600 2    50   UnSpc ~ 0
D6
Text HLabel 3350 3700 2    50   Output ~ 0
D7
Text HLabel 3350 3000 2    50   Output ~ 0
TX
Text HLabel 3350 1800 2    50   Input ~ 0
ADC0
Text HLabel 3350 1900 2    50   Input ~ 0
ADC1
Text HLabel 3350 2000 2    50   Input ~ 0
ADC2
Text HLabel 3350 2100 2    50   Input ~ 0
ADC3
Text HLabel 3350 2750 2    50   Input ~ 0
~RESET~
Text HLabel 3350 1150 2    50   Output ~ 0
D8
Text HLabel 3350 1250 2    50   Output ~ 0
D9
Text HLabel 3350 1350 2    50   Output ~ 0
D10(SS)
Text HLabel 3350 1450 2    50   Output ~ 0
D11(MOSI)
Text HLabel 3350 1550 2    50   Output ~ 0
D12(MISO)
Text HLabel 3350 1650 2    50   Output ~ 0
D13(SCK)
$Comp
L arduino_mini_pro:Arduino_Mini_Pro U1
U 1 1 5EDF1965
P 2750 2450
F 0 "U1" H 2221 2496 50  0000 R CNN
F 1 "Arduino_Mini_Pro" H 2221 2405 50  0000 R CNN
F 2 "TransistorTester-ProMini:Arduino_Pro_Mini" H 2750 2450 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8025-8-bit-AVR-Microcontroller-ATmega48P-88P-168P_Datasheet.pdf" H 2750 2450 50  0001 C CNN
	1    2750 2450
	1    0    0    -1  
$EndComp
Text HLabel 3350 2850 2    50   Input ~ 0
~RESET~
Text HLabel 3350 2200 2    50   Input ~ 0
ADC4(SDA)
Text HLabel 2750 3950 3    50   Input ~ 0
GND
Text HLabel 2750 950  1    50   Input ~ 0
VCC
Wire Wire Line
	5100 1800 5050 1800
Text HLabel 5100 1800 2    50   Input ~ 0
GND
Wire Wire Line
	4600 1800 4650 1800
Connection ~ 4600 1800
Wire Wire Line
	4600 1500 4600 1800
Wire Wire Line
	4550 1800 4600 1800
$Comp
L Switch:SW_Push SW1
U 1 1 5EE00B2E
P 4850 1800
F 0 "SW1" H 4850 2085 50  0000 C CNN
F 1 "SW_Push" H 4850 1994 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4850 2000 50  0001 C CNN
F 3 "~" H 4850 2000 50  0001 C CNN
	1    4850 1800
	1    0    0    -1  
$EndComp
Text HLabel 4550 1800 0    50   Input ~ 0
D7
$Comp
L Switch:SW_Push SW2
U 1 1 5EDD7CC2
P 4850 1950
F 0 "SW2" H 5150 2000 50  0000 C CNN
F 1 "SW_Push" H 5150 1900 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 4850 2150 50  0001 C CNN
F 3 "~" H 4850 2150 50  0001 C CNN
	1    4850 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 1950 4650 1800
Connection ~ 4650 1800
Wire Wire Line
	5050 1950 5050 1800
Connection ~ 5050 1800
$Comp
L Mechanical:MountingHole H8
U 1 1 5EDE615D
P 900 7900
F 0 "H8" H 1000 7946 50  0000 L CNN
F 1 "MountingHole" H 1000 7855 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 900 7900 50  0001 C CNN
F 3 "~" H 900 7900 50  0001 C CNN
	1    900  7900
	1    0    0    -1  
$EndComp
Text HLabel 8250 3300 2    50   Input ~ 0
GND
Text HLabel 8250 2300 2    50   Input ~ 0
VCC
Text HLabel 7750 2600 0    50   Input ~ 0
~RESET~
Text HLabel 8750 2800 2    50   Output ~ 0
D13(SCK)
Text HLabel 8750 2600 2    50   Output ~ 0
D12(MISO)
Text HLabel 8750 2700 2    50   Output ~ 0
D11(MOSI)
$Comp
L TransistorTester-ProMini:In-Circuit-Program J3
U 1 1 5EE7DC1C
P 8250 2800
F 0 "J3" H 8250 3481 50  0000 C CNN
F 1 "In-Circuit-Program" H 8250 3390 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 8400 2700 50  0001 C CNN
F 3 "" H 8100 2800 50  0001 C CNN
	1    8250 2800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
