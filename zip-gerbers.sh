#!/usr/bin/env bash

PROJ="TransistorTester-ProMini"
zip -9m ${PROJ}-gerber.zip *gbr* *drl
